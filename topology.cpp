#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/mobility-module.h"
#include "ns3/wifi-module.h"
#include "ns3/internet-module.h"
#include "ns3/applications-module.h"

using namespace ns3;

int main (int argc, char *argv[])
{
  // Create 3 sender nodes
  NodeContainer senders;
  senders.Create (3);
  
  // Create 3 receiver nodes
  NodeContainer receivers;
  receivers.Create (3);

  // Create the load balancer node
  NodeContainer loadBalancer;
  loadBalancer.Create (1);

  // Create the wifi channel and install the wifi protocol
  WifiHelper wifi;
  wifi.SetStandard (WIFI_STANDARD_80211ac);
  wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager",
                                "DataMode", StringValue ("OfdmRate54Mbps"),
                                "ControlMode", StringValue ("OfdmRate54Mbps"));
  YansWifiChannelHelper wifiChannel = YansWifiChannelHelper::Default();
  // added
  YansWifiPhyHelper phy;
  phy.SetErrorRateModel ("ns3::NistErrorRateModel");
  //
  wifiChannel.SetPropagationDelay ("ns3::ConstantSpeedPropagationDelayModel");
  wifiChannel.AddPropagationLoss ("ns3::FriisPropagationLossModel");


  //wifi.SetChannel (wifiChannel.Create ());
  phy.SetChannel (wifiChannel.Create ());
  WifiMacHelper mac;
  Ssid ssid = Ssid("ns-3-ssid");
//

  NetDeviceContainer senderDevices = wifi.Install (phy, mac, senders);
  NetDeviceContainer receiverDevices = wifi.Install (phy, mac, receivers);
  NetDeviceContainer loadBalancerDevices = wifi.Install (phy, mac, loadBalancer);

  // Install the UDP protocol on the sender nodes
  InternetStackHelper stack;
  stack.Install (senders);
  UdpClientHelper udpClient (Ipv4Address("192.168.1.1"), 9); // Fill in parameters for UdpClientHelper
  ApplicationContainer apps = udpClient.Install(senders);


  // Install the TCP protocol on the load balancer node
  stack.Install (loadBalancer);
  TcpServerHelper tcpServer (); // Fill in parameters for TcpServerHelper
  tcpServer.Install (loadBalancer);

  // Configure the random algorithm for the load balancer
  Ptr<LoadBalancer> lb = CreateObject<LoadBalancer> ();
  lb->SetAttribute ("Algorithm", StringValue ("Random"));
  lb->Install (loadBalancer.Get (0));

  // Create the links between the nodes
  Ipv4AddressHelper address;
  address.SetBase ("192.168.1.0", "255.255.255.0");
  Ipv4InterfaceContainer senderInterfaces = address.Assign (senderDevices);
  Ipv4InterfaceContainer receiverInterfaces = address.Assign (receiverDevices);
  Ipv4InterfaceContainer loadBalancerInterfaces = address.Assign (loadBalancerDevices);
  
  // ...

  // Set the positions of the nodes in a 2D space
MobilityHelper mobility;
mobility.SetPositionAllocator ("ns3::GridPositionAllocator",
"MinX", DoubleValue (0.0),
"MinY", DoubleValue (0.0),
"DeltaX", DoubleValue (5.0),
"DeltaY", DoubleValue (10.0),
"GridWidth", UintegerValue (3),
"LayoutType", StringValue ("RowFirst"));
mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
mobility.Install (senders);
mobility.Install (receivers);
mobility.Install (loadBalancer);

// Enable the packet trace
AsciiTraceHelper ascii;
wifiPhy.EnableAsciiAll (ascii.CreateFileStream ("wifi-udp-load-balancer.tr"));

// Run the simulation
Simulator::Run ();
Simulator::Destroy ();

return 0;

}

/*
This code creates a simulation of a wireless network with 3 sender nodes, 3 receiver nodes and 1 load balancer node. 
The wifi protocol is installed and configured on all the nodes, and the UDP protocol is installed on the sender nodes. T
he TCP protocol is installed on the load balancer node, and a load balancer object is created with a random algorithm. 
The nodes are positioned in a 2D space using the GridPositionAllocator and the ConstantPositionMobilityModel. 
The packet trace is enabled for the wifi protocol and the simulation is run. At the end of the simulation, the Simulator is destroyed.
*/