#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/applications-module.h"
#include "ns3/wifi-module.h"
#include <vector>

using namespace ns3;

class LoadBalancer {
public:
  LoadBalancer();
  void AddServer(Ptr<Node> server);
  void Start(Ptr<Node> client);

private:
  std::vector<Ptr<Node> > servers;
  void ForwardPacket(Ptr<const Packet> p, Ptr<Ipv4> ipv4, uint32_t iif);
};

LoadBalancer::LoadBalancer() {
}

void LoadBalancer::AddServer(Ptr<Node> server) {
  servers.push_back(server);
}

void LoadBalancer::Start(Ptr<Node> client) {
  Ipv4StaticRoutingHelper ipv4RoutingHelper;
  Ptr<Ipv4StaticRouting> clientStaticRouting = ipv4RoutingHelper.GetStaticRouting(client->GetObject<Ipv4>());
  clientStaticRouting->SetDefaultRoute(servers[0]->GetObject<Ipv4>()->GetAddress(1,0).GetLocal(), 2);

  Config::ConnectWithoutContext("/NodeList/*/DeviceList/*/Mac/MacRx", MakeCallback(&LoadBalancer::ForwardPacket, this));
}

void LoadBalancer::ForwardPacket(Ptr<const Packet> p, Ptr<Ipv4> ipv4, uint32_t iif) {
  Ipv4Header header;
  p->PeekHeader(header);
  Ptr<Node> srcNode = NodeList::GetNode(iif);
  Ipv4StaticRoutingHelper ipv4RoutingHelper;
  Ptr<Ipv4StaticRouting> srcStaticRouting = ipv4RoutingHelper.GetStaticRouting(srcNode->GetObject<Ipv4>());
  uint32_t nextHopIf = srcStaticRouting->GetOutputDevice(header.GetDestination());
  Ptr<NetDevice> outDev = srcNode->GetDevice(nextHopIf);
  Ptr<WifiNetDevice> wifiOutDev = outDev->GetObject<WifiNetDevice>();
  uint32_t randomServer = rand() % servers.size();
  Ptr<Ipv4> serverIpv4 = servers[randomServer]->GetObject<Ipv4>();
  srcStaticRouting->SetDefaultRoute(serverIpv4->GetAddress(1,0).GetLocal(), 2);
}
